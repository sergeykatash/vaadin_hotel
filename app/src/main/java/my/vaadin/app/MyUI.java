package my.vaadin.app;

import java.util.List;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.MultiSelectionModel;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;

import my.vaadin.app.backend.Category;
import my.vaadin.app.backend.Hotel;
import my.vaadin.app.backend.HotelService;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality
 */
@Theme("mytheme")
public class MyUI extends UI {
	
	private HotelService service = HotelService.getInstance();
	private MenuBar menu = new MenuBar();
	private Grid<Hotel> grid = new Grid<>(Hotel.class);
	private Grid<Category> gridCategories = new Grid<>(Category.class);
	private TextField filterName = new TextField();
	private TextField filterAddress = new TextField();
	private HotelForm form = new HotelForm(this);
	private CategoryForm formCategory = new CategoryForm(this);

    @Override
    protected void init(VaadinRequest vaadinRequest) {
    	final VerticalLayout layout = new VerticalLayout();
        
        MenuItem hotelsMenu = menu.addItem("Menu", null);
        hotelsMenu.addItem("Hotels", null, v -> {
        	setContent(hotelsPage(menu));
        });
        hotelsMenu.addItem("Categories of hotels", null, v -> {
        	setContent(categoryPage(menu));
        });
        
        layout.addComponent(hotelsPage(menu));
        setContent(layout);
   
    }
    
    // layout for hotels
    private Layout hotelsPage(MenuBar menu){
    	final VerticalLayout layoutHotel = new VerticalLayout();
    	
    	grid.setColumns("name","category", "address", "rating", "operatesFrom", "description");
        grid.addColumn(hotel ->"<a href='" + hotel.getUrl() + "' target='_blank'>more info</a>", new HtmlRenderer()).setCaption("Link");
        
        //search hotels by name
        filterName.setPlaceholder("filter by name...");
        filterName.addValueChangeListener(e -> updateListHotels());
        filterName.setValueChangeMode(ValueChangeMode.LAZY);
        
        Button clearFilterNameBtn = new Button(VaadinIcons.CLOSE);
        clearFilterNameBtn.setDescription("Clear the current filter");
        clearFilterNameBtn.addClickListener(e -> filterName.clear());
        
        CssLayout filteringN = new CssLayout();
        filteringN.addComponents(filterName, clearFilterNameBtn);
        filteringN.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
        
        //search hotels by address
        filterAddress.setPlaceholder("filter by address...");
        filterAddress.addValueChangeListener(e -> updateListHotels());
        filterAddress.setValueChangeMode(ValueChangeMode.LAZY);
        
        Button clearFilterAddressBtn = new Button(VaadinIcons.CLOSE);
        clearFilterAddressBtn.setDescription("Clear the current filter");
        clearFilterAddressBtn.addClickListener(e -> filterAddress.clear());
        
        CssLayout filteringAdd = new CssLayout();
        filteringAdd.addComponents(filterAddress, clearFilterAddressBtn);
        filteringAdd.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
        
        Button addHotelBtn = new Button("Add new hotel");
        addHotelBtn.addClickListener(e -> {
            grid.asSingleSelect().clear();
            form.setHotel(new Hotel());
        });
        
        HorizontalLayout toolbar = new HorizontalLayout(filteringN, filteringAdd, addHotelBtn);
        
        HorizontalLayout main = new HorizontalLayout(grid, form);
        main.setSizeFull();
        grid.setSizeFull();
        main.setExpandRatio(grid, 1);
        
        layoutHotel.addComponents(menu, toolbar, main);
        
        updateListHotels();
        
        form.setVisible(false);
        
        grid.asSingleSelect().addValueChangeListener(event -> {
            if (event.getValue() == null) {
                form.setVisible(false);
            } else {
                form.setHotel(event.getValue());
            }
        });
        
        return layoutHotel;
    	
    }
    
    // layout for categories of hotels
    private Layout categoryPage(MenuBar menu){
    	final VerticalLayout layoutCategory = new VerticalLayout();
    	HorizontalLayout mainCategories = new HorizontalLayout();
    	
    	// select "multi" mode for grid of categories
    	MultiSelectionModel<Category> selectionModel = (MultiSelectionModel<Category>) gridCategories
    			.setSelectionMode(SelectionMode.MULTI);
    	
    	gridCategories.setItems(service.getCategoriesL());
   
    	Button addCategoryBtn = new Button("Add new category");   	
        addCategoryBtn.addClickListener(e -> {
            gridCategories.asMultiSelect().clear();
            formCategory.setCategory(new Category());
        });    	
        
        Button editCategoryBtn = new Button("Edit category");  
        editCategoryBtn.addClickListener(e -> {
            formCategory.setCategory((Category)gridCategories.asMultiSelect().getSelectedItems().iterator().next());
        });
        
        Button deleteCategoryBtn = new Button("Delete category");  
        deleteCategoryBtn.addClickListener(e -> {
            service.deleteCategory(gridCategories.asMultiSelect().getSelectedItems());
            updateListCategories();
            updateListHotels();
        });
        
        //access to buttons
        selectionModel.addSelectionListener(v -> {
        	int count = selectionModel.getSelectedItems().size();
        	if (count == 0){
        		editCategoryBtn.setVisible(false); deleteCategoryBtn.setVisible(false);
        	}else{
        		if (count == 1){
        			editCategoryBtn.setVisible(true); deleteCategoryBtn.setVisible(true);
        		}else{
        			editCategoryBtn.setVisible(false); deleteCategoryBtn.setVisible(true);
        		}
        	}
        });
        
        //arrangement of components
        HorizontalLayout buttonsCategories = new HorizontalLayout();
        buttonsCategories.addComponents(addCategoryBtn, editCategoryBtn, deleteCategoryBtn);
    	mainCategories.addComponents(gridCategories, formCategory); 	
    	layoutCategory.addComponents(menu, mainCategories, buttonsCategories);
    	editCategoryBtn.setVisible(false); deleteCategoryBtn.setVisible(false);
    	updateListCategories();
    	formCategory.setVisible(false);
  	
    	return layoutCategory;
    }

	public void updateListHotels() {
		List<Hotel> hotels = service.findAll(filterName.getValue(), filterAddress.getValue());
        grid.setItems(hotels);
	}
	
	//update grid of categories
	public void updateListCategories() {
		List<Category> listOfCategories = service.getCategoriesL();
		gridCategories.setItems(listOfCategories);
		
	}

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }

}
