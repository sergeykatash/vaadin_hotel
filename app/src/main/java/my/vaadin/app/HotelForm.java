package my.vaadin.app;

import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import my.vaadin.app.backend.Category;
import my.vaadin.app.backend.Hotel;
import my.vaadin.app.backend.HotelService;
/**
*/
public class HotelForm extends FormLayout {
	
	private TextField name = new TextField("Name");
	private TextField address = new TextField("Address");
	private TextField rating = new TextField("Rating");
	private NativeSelect<Category> category = new NativeSelect<>("Category");
	private DateField operatesFrom = new DateField("OperatesFrom");
	private TextField url = new TextField("Url");
	private TextArea description = new TextArea("Description");
	private Button save = new Button("Save");
	private Button delete = new Button("Delete");
	private Binder<Hotel> binder = new Binder<>(Hotel.class);
	
	private HotelService service = HotelService.getInstance();
	private Hotel hotel;
	private MyUI myUI;
	
	public HotelForm(MyUI myUI) {
		this.myUI = myUI;
		
		name.setDescription("name of a hotel");
		address.setDescription("address of a hotel");
		rating.setDescription("rating of a hotel");
		category.setDescription("catogory of a hotel");
		operatesFrom.setDescription("date of start");
		url.setDescription("site of a hotel");
		description.setDescription("description of a hotel");
		
		setSizeUndefined();
		HorizontalLayout buttons = new HorizontalLayout(save, delete);
		addComponents(name, address, rating, category, operatesFrom, url, description , buttons);
		
		category.setItems(service.getCategoriesL());
		save.setStyleName(ValoTheme.BUTTON_PRIMARY);
		save.setClickShortcut(KeyCode.ENTER);

		bindFields();
				
		save.addClickListener(e -> this.save());
		delete.addClickListener(e -> this.delete());
		
	}
	
	private void bindFields(){
		binder.forField(rating).withConverter(new StringToIntegerConverter("Must be digits!")).asRequired("not be empty")
		.withValidator(v -> v>0, "Rating must contain at least one star" )
		.withValidator(v -> v<6, "5 is the max rating" )
		.bind(Hotel::getRating, Hotel::setRating);
		binder.forField(name).asRequired("not be empty").bind(Hotel::getName, Hotel::setName);
		binder.forField(address).asRequired("not be empty").bind(Hotel::getAddress, Hotel::setAddress);
		binder.forField(category).asRequired("not be empty").bind(Hotel::getCategory, Hotel::setCategory);
		binder.forField(url).asRequired("not be empty").bind(Hotel::getUrl, Hotel::setUrl);
		binder.forField(description).bind(Hotel::getDescription, Hotel::setDescription);
		binder.forField(operatesFrom).asRequired("not be empty").withConverter(new DateToLongConverter())
		.withValidator(v -> v>0, "not today")
		.bind(Hotel::getOperatesFrom, Hotel::setOperatesFrom);
	
		
	}
	
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
		binder.setBean(hotel);
		category.setItems(service.getCategoriesL());
		delete.setVisible(hotel.isPersisted());
	    setVisible(true);
	    name.selectAll();
	}
	
	private void delete() {
	    service.delete(hotel);
	    myUI.updateListHotels();
	    setVisible(false);
	}

	private void save() {
		service.save(hotel);
	    myUI.updateListHotels();
	    setVisible(false);
	}
	
	
	
	

}
