package my.vaadin.app;

import com.vaadin.data.Binder;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import my.vaadin.app.backend.Category;
import my.vaadin.app.backend.HotelService;
/**
*/
public class CategoryForm extends FormLayout{
	
	private TextField categoryField = new TextField("Category");
	private Button newCategory = new Button("Save");
	private Binder<Category> binder = new Binder<>(Category.class);
	
	private HotelService service = HotelService.getInstance();
	private Category category;
    private MyUI myUI;
	
	public CategoryForm(MyUI myUI) {
		this.myUI = myUI;
		
		categoryField.setDescription("category of a hotel");
		HorizontalLayout buttons = new HorizontalLayout(newCategory);
		addComponents(categoryField, buttons); 
		
		newCategory.setStyleName(ValoTheme.BUTTON_PRIMARY);
		newCategory.setClickShortcut(KeyCode.ENTER);
		
		//bind field with class category
		binder.forField(categoryField).asRequired("not be empty")
    	.bind(Category::getName, Category::setName);

		
		newCategory.addClickListener(e -> this.saveCategory());
		
	}
	
	public void setCategory(Category category) {
		this.category = category;
		binder.setBean(category);
		setVisible(true);
	}
	
	//save or edit category from form
	private void saveCategory() {
	    service.saveCategory(category);
	    myUI.updateListCategories();
	    setVisible(false);
	}
	
}
